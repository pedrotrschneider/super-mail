class_name EmailData
extends Resource

enum Actions {
	DELETE,
	ANSWER,
	ANSWER_ATTATCH,
	FORWARD,
}

enum Importance {
	LOW,
	MEDIUM,
	HIGH,
}

enum Properties {
	SENDER_NAME,
	SENDER_EMAIL,
	SUBJECT,
	CONTENTS,
	CORRECT_ACTION,
	IMPORTANCE,
}

export var sender_name: String = "";
export var sender_email: String = "";
export var subject: String = "";
export(String, MULTILINE) var contents: String = "";
export(Actions) var correct_action: int = 0;
export(Importance) var importance: int = 0;
