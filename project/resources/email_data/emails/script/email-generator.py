import csv

file_path = 'emails.csv'

with open(file_path, newline='', encoding='utf-8') as f:
    reader = csv.reader(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for row in reader:
        f = open(f"../email{row[0]}.tres", "w")
        f.write("")
        f.close()

        f = open(f"../email{row[0]}.tres", "a")
        f.write("[gd_resource type=\"Resource\" load_steps=2 format=2]\n")
        f.write("\n")
        f.write("[ext_resource path=\"res://resources/email_data/EmailData.gd\" type=\"Script\" id=1]\n")
        f.write("\n")
        f.write("[resource]\n")
        f.write("script = ExtResource( 1 )\n")
        f.write(f"sender_name = \"{row[2]}\"\n")
        f.write(f"sender_email = \"{row[1]}\"\n")
        f.write(f"subject = \"{row[3]}\"\n")
        f.write(f"contents = \"{row[5]}\"\n")
        f.write(f"correct_action = {row[6]}\n")
        f.write(f"importance = {row[4]}\n")
        f.close()

        print(f"Wrote e-mail {row[0]}")
        