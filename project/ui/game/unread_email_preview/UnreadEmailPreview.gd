tool
class_name UnreadEmailPreview
extends PanelContainer

export(String) var sender_name := "João Souza" setget set_sender_name;
export(String) var subject := "Ajuda com planejamento mensal" setget set_subject;


func set_sender_name(val: String) -> void:
	sender_name = val;
	$"%SenderName".text = val;


func set_subject(val: String) -> void:
	subject = val;
	$"%Subject".text = val
