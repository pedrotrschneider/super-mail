class_name Inbox
extends Control

export(PackedScene) var unread_email_preview_scene;

var simulation: Simulation;

# UI elements from the left panel
onready var next_emails_count_label: Label = $"%NextEmailsCountLabel";
onready var unread_emails_preview_container: VBoxContainer = $"%UnreadEmailsPreviewContainer";
onready var remaining_emails_count_label: Label = $"%RemainingEmailsCountLabel";

# UI elements from the center panel
onready var current_email_timer_label: Label = $"%CurrentEmailTimerLabel";
onready var score_label: Label = $"%ScoreLabel";
onready var from_label: Label = $"%FromLabel";
onready var current_subject_label: Label = $"%CurrentSubjectLabel";
onready var current_sender_name_label: Label = $"%CurrentSenderNameLabel";
onready var current_sender_email_label: Label = $"%CurrentSenderEmailLabel";
onready var current_contents_label: RichTextLabel = $"%CurrentContentsLabel";
# missing the week day indicator

# UI elements from the right panel
onready var deleted_count_label: Label = $"%DeletecCountLabel";
onready var answered_count_label: Label = $"%AnsweredCountLabel";
onready var answered_attatch_count_label: Label = $"%AnsweredAttatchCountLabel";
onready var forwarded_count_label: Label = $"%ForwadedCountLabel";
onready var correct_count_label: Label = $"%CorrectCountLabel";
onready var incorrect_count_label: Label = $"%IncorrectCountLabel";
onready var pending_count_label: Label = $"%PendingCountLabel";

var target_score: int = 0;
var current_score: int = 0;

func _ready() -> void:
	hide_current_email();
	
	# warning-ignore:return_value_discarded
	simulation.connect("correct_answer", self, "_on_simulation_correct_answer");
	# warning-ignore:return_value_discarded
	simulation.connect("incorrect_answer", self, "_on_simulation_incorrect_answer");
	# warning-ignore:return_value_discarded
	simulation.connect("current_email_update", self, "_on_simulation_current_email_update");
	# warning-ignore:return_value_discarded
	simulation.connect("day_over", self, "_on_simulation_day_over");
	
	# warning-ignore:return_value_discarded
	$"%DeleteButton".connect("pressed", self, "_on_answer_button_pressed");
	# warning-ignore:return_value_discarded
	$"%AnswerButton".connect("pressed", self, "_on_answer_button_pressed");
	# warning-ignore:return_value_discarded
	$"%AnswerAttatchmentButton".connect("pressed", self, "_on_answer_button_pressed");
	# warning-ignore:return_value_discarded
	$"%ForwardButton".connect("pressed", self, "_on_answer_button_pressed");
	
	# warning-ignore:return_value_discarded
	$FullscreenButton.connect("pressed", self, "_on_fullscreen_button_pressed");
	# warning-ignore:return_value_discarded
	$CloseButton.connect("pressed", self, "_on_close_button_pressed");
	# warning-ignore:return_value_discarded
	$StartButton.connect("pressed", self, "_on_start_button_pressed");


func _process(delta: float) -> void:
	var current_timer: PoolStringArray = str(simulation.get_current_timer()).split(".");
	var secs: String = current_timer[0];
	var milis: String = current_timer[1].substr(0,2);
	current_email_timer_label.text = "%s:%s" % [secs, milis];
	
	
	var rate: float = 1 - pow(2, -10 * delta);
	current_score += int(ceil((float(target_score) - float(current_score)) * rate));
	score_label.text = str(current_score);


func _on_answer_button_pressed(idx: int) -> void:
	simulation.answer_curret_email(idx);


func _on_simulation_correct_answer() -> void:
	pass;


func _on_simulation_incorrect_answer() -> void:
	pass;


func _on_simulation_current_email_update() -> void:
	var unread_emails_count: int = simulation.get_unread_emails_count();
	next_emails_count_label.text = "(%d)" % unread_emails_count;
	if unread_emails_count > 4:
		remaining_emails_count_label.text = "Mais %d emails..." % (unread_emails_count - 4);
	else:
		remaining_emails_count_label.hide();
	
	for c in unread_emails_preview_container.get_children():
		c.queue_free();
	
	for i in min(unread_emails_count, 4):
		var new_uep: UnreadEmailPreview = unread_email_preview_scene.instance();
		new_uep.sender_name = simulation.get_unread_email_sender_name(i);
		new_uep.subject = simulation.get_unread_email_subject(i);
		unread_emails_preview_container.add_child(new_uep);
	
	current_subject_label.text = simulation.get_current_subject();
	current_sender_name_label.text = simulation.get_current_sender_name();
	current_sender_email_label.text = "<%s>" % simulation.get_current_sender_email();
	current_contents_label.bbcode_text = simulation.get_current_contents();
	
	deleted_count_label.text = "(%d)" % simulation.get_deleted_count();
	answered_count_label.text = "(%d)" % simulation.get_answered_count();
	answered_attatch_count_label.text = "(%d)" % simulation.get_answered_attatch_count();
	forwarded_count_label.text = "(%d)" % simulation.get_forwarded_count();
	
	correct_count_label.text = "%d" % simulation.get_correct_answers_count();
	incorrect_count_label.text = "%d" % simulation.get_incorrect_answers_count();
	pending_count_label.text = "%d" % simulation.get_pending_emails_count();
	
	target_score = simulation.get_score();


func _on_simulation_day_over() -> void:
	hide_current_email();


func _on_fullscreen_button_pressed() -> void:
	OS.window_fullscreen = !OS.window_fullscreen;


func _on_close_button_pressed() -> void:
	self.get_tree().quit();


func _on_start_button_pressed() -> void:
	$StartButton.queue_free();
	$AnimationPlayer.play("fadeout");
	yield($AnimationPlayer, "animation_finished");
	$Bg.queue_free();
	$AnimationPlayer.queue_free();
	simulation.switch_state(Simulation.States.PLAYING);
	show_current_email();


func hide_current_email() -> void:
	current_subject_label.hide();
	from_label.hide();
	current_sender_name_label.hide();
	current_sender_email_label.hide();
	current_contents_label.hide();


func show_current_email() -> void:
	current_subject_label.show();
	from_label.show();
	current_sender_name_label.show();
	current_sender_email_label.show();
	current_contents_label.show();
