tool
extends PanelContainer

signal pressed(answer_index);

export(Texture) var texture: Texture setget set_texture;
export(Color) var main_color: Color;
export(Color) var disabled_color: Color;
export(Color) var hover_overlay_color: Color;
export(Color) var pressed_overlay_color: Color;
export(float) var hover_scale: float;
export(float) var pressed_scale: float;
export(float) var color_lerp_speed: float = 10.0;
export(bool) var disabled: bool = false;
export(int) var answer_index: int = 0;

var has_mouse: bool = false;
var is_pressed: bool = false;

func _ready() -> void:
	# warning-ignore:return_value_discarded
	self.connect("mouse_entered", self, "_on_mouse_entered");
	# warning-ignore:return_value_discarded
	self.connect("mouse_exited", self, "_on_mouse_exited");
	# warning-ignore:return_value_discarded
	$Button.connect("pressed", self, "_on_button_pressed");


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if has_mouse and event.is_pressed():
			is_pressed = true;
		elif is_pressed:
			is_pressed = false;
			yield(self.get_tree(), "idle_frame");
			if has_mouse and not disabled:
#				self.emit_signal("pressed", answer_index);
				pass


func _process(delta: float) -> void:
	self.rect_pivot_offset = self.rect_size / 2.0;
	
	var target_overlay_color: Color = Color(1.0, 1.0, 1.0, 0.0);
	var target_main_color: Color = main_color;
	var target_scale: float = 1.0;
	
	if not disabled:
		if has_mouse:
			target_overlay_color = hover_overlay_color;
			target_scale = hover_scale;
			if is_pressed:
				target_overlay_color = pressed_overlay_color;
				target_scale = pressed_scale;
	else:
		target_main_color = disabled_color;
	
	var overlay_stylebox = $OverlayPanel.get_stylebox("panel").duplicate();
	overlay_stylebox.bg_color = lerp(overlay_stylebox.bg_color, target_overlay_color, color_lerp_speed * delta);
	$OverlayPanel.add_stylebox_override("panel", overlay_stylebox);
	
	var main_stylebox = self.get_stylebox("panel").duplicate();
	main_stylebox.bg_color = lerp(main_stylebox.bg_color, target_main_color, color_lerp_speed * delta);
	self.add_stylebox_override("panel", main_stylebox);
	
	self.rect_scale = lerp(self.rect_scale, Vector2(target_scale, target_scale), color_lerp_speed * delta);


func _on_mouse_entered() -> void:
	has_mouse = true;


func _on_mouse_exited() -> void:
	has_mouse = false;


func _on_button_pressed() -> void:
	self.emit_signal("pressed", answer_index);


func set_texture(val: Texture) -> void:
	texture = val;
	if self.has_node("MarginContainer/TextureRect"):
		$MarginContainer/TextureRect.texture = val;


func set_main_color(val: Color) -> void:
	main_color = val;
	var stylebox := self.get_stylebox("panel").duplicate();
	stylebox.bg_color = val;
	self.add_stylebox_override("panel", stylebox);
