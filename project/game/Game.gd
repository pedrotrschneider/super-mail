extends Node

export(PackedScene) var simulation_scene;
export(PackedScene) var inbox_scene;

var simulation: Simulation;
var inbox: Inbox;


func _ready() -> void:
	yield(self.get_tree(), "idle_frame");
	simulation = simulation_scene.instance();
	inbox = inbox_scene.instance();
	inbox.simulation = simulation;
	self.add_child(simulation);
	self.add_child(inbox);


func _input(event: InputEvent) -> void:
	if event is InputEventKey and event.is_pressed():
		if event.scancode == KEY_F:
			OS.window_fullscreen = not OS.window_fullscreen;
