class_name Email
extends Node

var email_data: EmailData;
var sender_name: String;
var sender_email: String;
var subject: String;
var contents: String;
var correct_action: int;
var importance: int;


func import_data(data: EmailData) -> void:
	sender_name = data.sender_name;
	sender_email = data.sender_email;
	subject = data.subject;
	contents = data.contents;
	correct_action = data.correct_action;
	importance = data.importance;
