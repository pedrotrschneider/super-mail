class_name Simulation
extends Node

signal state_switched(new_state);
signal correct_answer();
signal incorrect_answer();
signal current_email_update();
signal day_over();

enum States {
	WAITING,
	PLAYING,
	DAY_OVER
}

const BASE_SCORE: int = 3000;

export(PackedScene) var email_scene;

onready var unread_emails_node: Node = $UnreadEmails;
onready var current_email_node: Node = $CurrentEmail;
onready var correct_emails_node: Node = $CorrectEmails;
onready var incorrect_emails_node: Node = $IncorrectEmails;

var deleted_count: int = 0;
var answered_count: int = 0;
var answered_attatch_count: int = 0;
var forwarded_count: int = 0;
var current_timer: float = 0.00001;
var score: int = 0;

var state: int = States.WAITING;


func _ready() -> void:
	var email_data: Array = get_all_email_data();
	load_emails(email_data);


func _process(delta: float) -> void:
	match state:
		States.WAITING:
			pass
		States.PLAYING:
			current_timer += delta;
		States.DAY_OVER:
			pass


func switch_state(new_state: int) -> void:
	state = new_state;
	self.emit_signal("state_switched", new_state);
	
	match new_state:
		States.WAITING:
			return;
		States.PLAYING:
			update_current();
		States.DAY_OVER:
			return;


func answer_curret_email(action: int) -> void:
	match state:
		States.WAITING:
			pass
		
		States.PLAYING:
			var current_email: Email = current_email_node.get_child(0);
			current_email_node.remove_child(current_email);
			
			match action:
				EmailData.Actions.DELETE:
					deleted_count += 1;
				EmailData.Actions.ANSWER:
					answered_count += 1;
				EmailData.Actions.ANSWER_ATTATCH:
					answered_attatch_count += 1;
				EmailData.Actions.FORWARD:
					forwarded_count += 1;
			
			if current_email.correct_action == action:
				correct_emails_node.add_child(current_email);
				score += calculate_score(current_email);
				self.emit_signal("correct_answer");
				print("[Backend] Correct answer");
			else:
				incorrect_emails_node.add_child(current_email);
				self.emit_signal("incorrect_answer");
				print("[Backend] Wrong answer");
			update_current();
		
		States.DAY_OVER:
			return;


func calculate_score(current_email: Email) -> int:
	var importance_mult: int = current_email.importance + 1;
	var timer_mult: float = exp(-current_timer / 5);
	
	return int(BASE_SCORE * importance_mult * timer_mult);


func update_current() -> void:
	current_timer = 0.00001;
	if unread_emails_node.get_child_count() > 0:
		var new_current: Email = unread_emails_node.get_child(0);
		unread_emails_node.remove_child(new_current);
		current_email_node.add_child(new_current);
	else:
		switch_state(States.DAY_OVER);
		self.emit_signal("day_over");
		print("[Backend] Day over");
	self.emit_signal("current_email_update");


func load_emails(email_data: Array) -> void:
	email_data.shuffle();
	for ed in email_data:
		var new_email: Email = email_scene.instance();
		new_email.import_data(ed);
		unread_emails_node.add_child(new_email);


func get_all_email_data() -> Array:
	var emails_path: String = "res://resources/email_data/emails/";
	var file_names: Array = [];
	var dir: Directory = Directory.new();
	# warning-ignore:return_value_discarded
	dir.open(emails_path);
	# warning-ignore:return_value_discarded
	dir.list_dir_begin();
	
	while true:
		var file = dir.get_next();
		if file == "":
			break;
		elif file.ends_with(".tres"):
			file_names.append(file);
	
	dir.list_dir_end();
	
	var emails_data: Array = [];
	for file_name in file_names:
		emails_data.append(load(emails_path + file_name));
	return emails_data;


func get_deleted_count() -> int:
	return deleted_count;


func get_answered_count() -> int:
	return answered_count;


func get_answered_attatch_count() -> int:
	return answered_attatch_count;


func get_forwarded_count() -> int:
	return forwarded_count;


func get_current_timer() -> float:
	return current_timer;


func get_score() -> int:
	return score;


func get_current_sender_name() -> String:
	if current_email_node.get_child_count() > 0:
		return (current_email_node.get_child(0) as Email).sender_name;
	return "";


func get_current_sender_email() -> String:
	if current_email_node.get_child_count() > 0:
		return (current_email_node.get_child(0) as Email).sender_email;
	return "";


func get_current_subject() -> String:
	if current_email_node.get_child_count() > 0:
		return (current_email_node.get_child(0) as Email).subject;
	return "";


func get_current_contents() -> String:
	if current_email_node.get_child_count() > 0:
		return (current_email_node.get_child(0) as Email).contents;
	return "";


func get_current_correct_action() -> int:
	if current_email_node.get_child_count() > 0:
		return (current_email_node.get_child(0) as Email).correct_action;
	return -1;


func get_current_importance() -> int:
	if current_email_node.get_child_count() > 0:
		return (current_email_node.get_child(0) as Email).importance;
	return -1;


func get_unread_email_sender_name(i: int) -> String:
	return (unread_emails_node.get_child(i) as Email).sender_name;


func get_unread_email_subject(i: int) -> String:
	return (unread_emails_node.get_child(i) as Email).subject;


func get_correct_answers_count() -> int:
	return correct_emails_node.get_child_count();


func get_incorrect_answers_count() -> int:
	return incorrect_emails_node.get_child_count();


func get_unread_emails_count() -> int:
	return unread_emails_node.get_child_count();


func get_pending_emails_count() -> int:
	return get_unread_emails_count() + current_email_node.get_child_count();
